'use strict';

const Product = require('../products/products.model');
module.exports = {
    index: (req, res) => {
        Product
        .find({})
        .exec((err, ProductDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Product Details fetched Successfully", data : ProductDetails});
        })
    },
    retrieve: (req, res) => {
        const ProductId = req.params.id;
        Product
        .findOne({_id:ProductId})
        .exec((err, ProductDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Product Detail fetched Successfully", data : ProductDetails});
        })
    },
    retrieve: (req, res) => {
        const IdUser = req.params.id;
        Product
        .find({id_user:IdUser})
        .exec((err, ProductDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Product Detail fetched Successfully", data : ProductDetails});
        })
    },
    create: (req, res) => {
        Product.create(req.body, (err, ProductDetails) => {
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(201).json({ message: "Product Created Successfully", data : ProductDetails});
        })  
    },
    update: (req, res)=>{
        const ProductId = req.params.id;
        Product
        .findByIdAndUpdate(ProductId, { $set: req.body }).exec((err, ProductDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Product updated",data:ProductDetails });
        })
    },
    delete: (req, res)=>{
        const ProductId = req.params.id;
        Product
        .findByIdAndDelete(ProductId, { $set: { is_active: false } }).exec((err, ProductDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Product Deleted" });
        })
    }
}