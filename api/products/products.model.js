'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ProductSchema = new Schema({
    id_user:{
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    unit: {
        type: String,
        required: true
    },
    priceBasedOn: {
        type: String,
        required: true
    },
    priceWT: {
        type: Number,
        required: true
    },
    vat: {
        type: Number,
        required: true
    },
    priceTTC:{
        type:Number,
        required:true
    }
},{
    id: false,
    toObject: {
        virtuals: true,
        getters: true
    },
    toJSON: { 
        virtuals: true,
        getters: true, 
        setters: false 
    },
    timestamps: true
});

ProductSchema.pre('find', function () {
    this.where({ is_active: { $ne: false } });
});

module.exports = mongoose.model('Product', ProductSchema);