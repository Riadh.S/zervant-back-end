'use strict';

const Client = require('../clients/clients.model');
module.exports = {
    index: (req, res) => {
        Client
        .find({})
        .exec((err, ClientDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Client Details fetched Successfully", data : ClientDetails});
        })
    },
    retrieve: (req, res) => {
        const ClientId = req.params.id;
        Client
        .findOne({_id:ClientId})
        .exec((err, ClientDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Client Detail fetched Successfully", data : ClientDetails});
        })
    },
    retrieve: (req, res) => {
        const IdUser = req.params.id;
        Client
        .find({idUser:IdUser })
        .exec((err, ClientDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Client Detail fetched Successfully", data : ClientDetails});
        })
    },
    create: (req, res) => {
        console.log(req.body);
        Client.create(req.body, (err, ClientDetails) => {
        
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(201).json({ message: "Client Created Successfully", data : ClientDetails});
        })
    },
    update: (req, res)=>{
        const ClientId = req.params.id;
        Client
        .findByIdAndUpdate(ClientId, { $set: req.body }).exec((err, ClientDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Client updated" });
        })
    },
    delete: (req, res)=>{
        const ClientId = req.params.id;
        Client
        .findByIdAndDelete(ClientId, { $set: { is_active: false } }).exec((err, ClientDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Client Deleted" });
        })
    }
}