'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ClientSchema = new Schema({
    idUser:{
        type: String,
        required: true
    },
    sendMethod: {
        type: String,
        required: true
    },
    typeClient: {
        type: String,
        required: true
    },
    companyName: {
        type: String,
        required: true
    },
    siretNumber: {
        type: Number,
        required: true
    },
    vatNumber:{
        type: Number,
        required: true 
    },
   title:{
    type: String,
    required: true
    },
    firstName:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    phoneNumber:{
        type: Number,
        required: true
    },
    companyNumber:{
        type: Number,
        required: true
    },
    adress:{
        type: String,
        required: true
    },
   codePostal:{
    type: Number,
    required: true
    },
    city:{
        type: String,
        required: true
    },
    country:{
        type: String,
        required: true
    },
    language:{
        type: String,
        required: true
    },
    paymentCondition:{
        type: String,
    required: true
    },
    adressEbill:{
        typeAdress:{
            type: String,
        required: false
        },
        valueAdress:{
            type: Number,
        required: false
        }
    },
    operatorEbill:{
        type: String,
        required: true
    },
    networkEbill:{
        type: String,
        required: true
    }
},{
    id: false,
    toObject: {
        virtuals: true,
        getters: true
    },
    toJSON: { 
        virtuals: true,
        getters: true, 
        setters: false 
    },
    timestamps: true
});

ClientSchema.pre('find', function () {
    this.where({ is_active: { $ne: false } });
});

module.exports = mongoose.model('Client', ClientSchema);