'use strict';

const Bill = require('../bills/bills.model');
module.exports = {
    index: (req, res) => {
        Bill
        .find({})
        .exec((err, BillDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Bill Details fetched Successfully", data : BillDetails});
        })
    },
    retrieve: (req, res) => {
        const BillId = req.params.id;
        Bill
        .findOne({_id:BillId})
        .exec((err, BillDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Bill Detail fetched Successfully", data : BillDetails});
        })
    },
    retrieve: (req, res) => {
        const IdUser = req.params.id;
        Bill
        .find({id_user:IdUser})
        .exec((err, BillDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Bill Detail fetched Successfully", data : BillDetails});
        })
    },
    create: (req, res) => {
        Bill.create(req.body, (err, BillDetails) => {
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(201).json({ message: "Bill Created Successfully", data : BillDetails});
        })
    },
    update: (req, res)=>{
        const BillId = req.params.id;
        Bill
        .findByIdAndUpdate(BillId, { $set: req.body }).exec((err, BillDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Bill updated" });
        })
    },
    delete: (req, res)=>{
        const BillId = req.params.id;
        Bill
        .findByIdAndDelete(BillId, { $set: { is_active: false } }).exec((err, BillDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Bill Deleted" });
        })
    }
}