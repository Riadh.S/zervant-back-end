'use strict';

const Devis = require('../devis/devis.model');
module.exports = {
    index: (req, res) => {
        Devis
        .find({})
        .exec((err, DevisDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Devis Details fetched Successfully", data : DevisDetails});
        })
    },
    retrieve: (req, res) => {
        const DevisId = req.params.id;
        Devis
        .findOne({_id:DevisId})
        .exec((err, DevisDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Devis Detail fetched Successfully", data : DevisDetails});
        })
    },
    retrieve: (req, res) => {
        const IdUser = req.params.id;
        Devis
        .findOne({id_user:IdUser})
        .exec((err, DevisDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Devis Detail fetched Successfully", data : DevisDetails});
        })
    },
    create: (req, res) => {
        Devis.create(req.body, (err, DevisDetails) => {
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(201).json({ message: "Devis Created Successfully", data : DevisDetails});
        })
    },
    update: (req, res)=>{
        const DevisId = req.params.id;
        Devis
        .findByIdAndUpdate(DevisId, { $set: req.body }).exec((err, DevisDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Devis updated" });
        })
    },
    delete: (req, res)=>{
        const DevisId = req.params.id;
        Devis
        .findByIdAndDelete(DevisId, { $set: { is_active: false } }).exec((err, DevisDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Devis Deleted" });
        })
    }
}