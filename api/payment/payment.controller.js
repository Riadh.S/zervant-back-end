'use strict';

const Payment = require('../payment/payment.model');
module.exports = {
    index: (req, res) => {
        Payment
        .find({})
        .exec((err, PaymentDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Payment Details fetched Successfully", data : PaymentDetails});
        })
    },
    retrieve: (req, res) => {
        const PaymentId = req.params.id;
        Payment
        .findOne({_id:PaymentId})
        .exec((err, PaymentDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Payment Detail fetched Successfully", data : PaymentDetails});
        })
    },
    retrieve: (req, res) => {
        const IdUser = req.params.id;
        Payment
        .findOne({id_user:IdUser})
        .exec((err, PaymentDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Payment Detail fetched Successfully", data : PaymentDetails});
        })
    },
    create: (req, res) => {
        Payment.create(req.body, (err, PaymentDetails) => {
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(201).json({ message: "Payment Created Successfully", data : PaymentDetails});
        })
    },
    update: (req, res)=>{
        const PaymentId = req.params.id;
        Payment
        .findByIdAndUpdate(PaymentId, { $set: req.body }).exec((err, PaymentDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Payment updated" });
        })
    },
    delete: (req, res)=>{
        const PaymentId = req.params.id;
        Payment
        .findByIdAndDelete(PaymentId, { $set: { is_active: false } }).exec((err, PaymentDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Payment Deleted" });
        })
    }
}