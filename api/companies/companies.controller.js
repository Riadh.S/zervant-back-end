'use strict';

const Company = require('../companies/companies.model');
module.exports = {
    index: (req, res) => {
        Company
        .find({})
        .exec((err, CompanyDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Company Details fetched Successfully", data : CompanyDetails});
        })
    },
    retrieve: (req, res) => {
        const CompanyId = req.params.id;
        Company
        .findOne({_id:CompanyId})
        .exec((err, CompanyDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Company Detail fetched Successfully", data : CompanyDetails});
        })
    },
    retrieve: (req, res) => {
        const IdUser = req.params.id;
        Company
        .findOne({id_user:IdUser})
        .exec((err, CompanyDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Company Detail fetched Successfully", data : CompanyDetails});
        })
    },
    create: (req, res) => {
        Company.create(req.body, (err, CompanyDetails) => {
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(201).json({ message: "Company Created Successfully", data : CompanyDetails});
        })
    },
    update: (req, res)=>{
        const CompanyId = req.params.id;
        Company
        .findByIdAndUpdate(CompanyId, { $set: req.body }).exec((err, CompanyDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Company updated" });
        })
    },
    delete: (req, res)=>{
        const CompanyId = req.params.id;
        Company
        .findByIdAndDelete(CompanyId, { $set: { is_active: false } }).exec((err, CompanyDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Company Deleted" });
        })
    }
}