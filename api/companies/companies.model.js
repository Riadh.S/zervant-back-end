'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const CompanySchema = new Schema({
    id_user:{
        type: String,
        required: true
    },
    siretNumber: {
        type: Number,
        required: true
    },
    vatNumber: {
        type: Number,
        required: true
    },
    vatDeclaration: {
        type: String,
        required: true
    },
    defaultVat: {
        type: Number,
        required: true
    },
    adress:{
        type: String,
        required: false 
    },
   codePostal:{
    type: Number,
    required: false 
    },
    city:{
        type: String,
        required: false 
    },
    webSite:{
        type: String,
        required: false 
    },
    logo:{
        type: String,
        required: false 
    }
    
},{
    id: false,
    toObject: {
        virtuals: true,
        getters: true
    },
    toJSON: { 
        virtuals: true,
        getters: true, 
        setters: false 
    },
    timestamps: true
});

CompanySchema.pre('find', function () {
    this.where({ is_active: { $ne: false } });
});

module.exports = mongoose.model('Company', CompanySchema);