'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const BillSettingSchema = new Schema({
    id_user:{
        type: String,
        required: true
    },
    companyName:{
        type: String,
        required: true 
    },    
    currency:{
        type: String,
        required: true 
    },
    language:{
        type: String,
        required: true  
    },
    billHeader: {
        type: String,
        required: true
    },
    billType: {
        type: String,
        required: true
    },
    paymentCondition: {
        valuePD:{
        type: Number,
        required: true
        },
        displayPD:{
            type: Boolean,
            default:false
            }
    },
    delayInterest: {
      valueDI:{
        type: Number,
        required: true
      },
      displayDI:
      {
        type: Boolean,
        default:false
      }
    },
    paymentRef:{
        type: Boolean,
        default: false 
    },
   attentionOf:{
    type: Boolean,
    default: false 
    },
    displayColunms:
    {
        name: {
           type:Boolean,
           default:true
        },
        description: {
           type:Boolean,
           default:true
        },
        unit: {
           type:Boolean,
           default:true
        },
        priceBasedOn: {
           type:Boolean,
           default:true
        },
        priceWT: {
            type:Boolean,
            default:true
        },
        vat: {
            type:Boolean,
            default:true
        },
        priceTTC:{
            type:Boolean,
            default:true
        }
    },
    discountSetting:{
        Totel: {
            type:Boolean,
            default:true
         },
        withRow: {
            type:Boolean,
            default:true
         },
         price: {
            type:Boolean,
            default:true
         },
         percent: {
            type:Boolean,
            default:true
         },
         BaseHT: {
             type:Boolean,
             default:true
         },
         BaseTTC: {
             type:Boolean,
             default:true
         },
        displayDS:{
             type:Boolean,
             default:true
         }   
    },
    message:{
        type: String,
        required: false 
    },
    notesFooter:{
        type: String,
        required: false 
    },
    
},{
    id: false,
    toObject: {
        virtuals: true,
        getters: true
    },
    toJSON: { 
        virtuals: true,
        getters: true, 
        setters: false 
    },
    timestamps: true
});

BillSettingSchema.pre('find', function () {
    this.where({ is_active: { $ne: false } });
});

module.exports = mongoose.model('BillSetting', BillSettingSchema);