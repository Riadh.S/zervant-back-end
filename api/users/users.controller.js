'use strict';

const User = require('../users/users.model');
const jwt = require('jsonwebtoken');
const config = require('../../config/environment');
module.exports = {
    index: (req, res) => {
       User
        .find({})
        .exec((err,UserDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "User Details fetched Successfully", data :UserDetails});
        })
    },
    retrieve: (req, res) => {
        const UserId = req.params.id;
       User
        .findOne({_id:UserId})
        .exec((err,UserDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "User Detail fetched Successfully", data :UserDetails});
        })
    },
    create: (req, res) => {
        console.log(req.body);
       User.create(req.body, (err,UserDetails) => {
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }else{
                let payload = {
                    user_id : UserDetails._id,
                    email:UserDetails.email
                }
    
                console.log(config.secrets.session);
                let token = jwt.sign(payload, config.secrets.session,{
                    expiresIn : config.secrets.expiresIn
                });
                
                return res.status(200).json({auth : true, token : token, message : "User Logged In Successfully"});  
            }
          
        })
    },
    update: (req, res)=>{
        const UserId = req.params.id;
       User
        .findByIdAndUpdate(UserId, { $set: req.body }).exec((err,UserDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "User updated" });
        })
    },
    delete: (req, res)=>{
        const UserId = req.params.id;
       User
        .findByIdAndDelete(UserId, { $set: { is_active: false } }).exec((err,UserDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "User Deleted" });
        })
    }
}