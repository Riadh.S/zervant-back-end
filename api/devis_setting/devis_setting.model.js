'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const DevisSettingSchema = new Schema({
    id_user:{
        type: String,
        required: true
    },
    companyName:{
        type: String,
        required: false 
    },    
    currency:{
        type: String,
        required: true 
    },
    language:{
        type: String,
        required: true  
    },
    devisHeader: {
        type: String,
        required: true
    },
    dayValidity: {
      
        type: Boolean,
        default:false
    },
   attentionOf:{
    type: Boolean,
    default: false 
    },
    displayColunms:
    {
        name: {
           type:Boolean,
           default:true
        },
        description: {
           type:Boolean,
           default:true
        },
        unit: {
           type:Boolean,
           default:true
        },
        priceBasedOn: {
           type:Boolean,
           default:true
        },
        priceWT: {
            type:Boolean,
            default:true
        },
        vat: {
            type:Boolean,
            default:true
        },
        priceTTC:{
            type:Boolean,
            default:true
        }
    },
    discountSetting:{
        Totel: {
            type:Boolean,
            default:true
         },
        withRow: {
            type:Boolean,
            default:true
         },
         price: {
            type:Boolean,
            default:true
         },
         percent: {
            type:Boolean,
            default:true
         },
         BaseHT: {
             type:Boolean,
             default:true
         },
         BaseTTC: {
             type:Boolean,
             default:true
         },
        displayDS:{
             type:Boolean,
             default:true
         }   
    },
    message:{
        type: String,
        required: false 
    },
    notesFooter:{
        type: String,
        required: false 
    },
    ConditionsSale:{
        valueCS:{
            type: String,
            required: false 
        },
        displayCS:{
            type:Boolean,
            default:true  
        }
    }
    
},{
    id: false,
    toObject: {
        virtuals: true,
        getters: true
    },
    toJSON: { 
        virtuals: true,
        getters: true, 
        setters: false 
    },
    timestamps: true
});

DevisSettingSchema.pre('find', function () {
    this.where({ is_active: { $ne: false } });
});

module.exports = mongoose.model('DevisSetting', DevisSettingSchema);