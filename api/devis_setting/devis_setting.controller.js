'use strict';

const DevisSetting = require('../devis_setting/devis_setting.model');
module.exports = {
    index: (req, res) => {
        DevisSetting
        .find({})
        .exec((err, DevisSettingDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "DevisSetting Details fetched Successfully", data : DevisSettingDetails});
        })
    },
    retrieve: (req, res) => {
        const DevisSettingId = req.params.id;
        DevisSetting
        .findOne({_id:DevisSettingId})
        .exec((err, DevisSettingDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "DevisSetting Detail fetched Successfully", data : DevisSettingDetails});
        })
    },
    retrieve: (req, res) => {
        const IdUser = req.params.id;
        DevisSetting
        .findOne({id_user:IdUser})
        .exec((err, DevisSettingDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "DevisSetting Detail fetched Successfully", data : DevisSettingDetails});
        })
    },
    create: (req, res) => {
        DevisSetting.create(req.body.DevisSetting, (err, DevisSettingDetails) => {
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(201).json({ message: "DevisSetting Created Successfully", data : DevisSettingDetails});
        })
    },
    update: (req, res)=>{
        const DevisSettingId = req.params.id;
        DevisSetting
        .findByIdAndUpdate(DevisSettingId, { $set: req.body }).exec((err, DevisSettingDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "DevisSetting updated" });
        })
    },
    delete: (req, res)=>{
        const DevisSettingId = req.params.id;
        DevisSetting
        .findByIdAndDelete(DevisSettingId, { $set: { is_active: false } }).exec((err, DevisSettingDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "DevisSetting Deleted" });
        })
    }
}